try:
    import pygame_sdl2
    pygame_sdl2.import_as_pygame()
except ImportError:
    pass
import pygame
from loadImg import transp, platform, hSurface, wSurface

clPlein = (150,150,150)
clVide = (100, 100, 100)



class Arme:

    def __init__(self, nom="", rarete="",  att="", img="", shiny="", gold=""):

        """
        :param nom: String
        :param rarete: Dictionnaire
        :param att: Int
        :param img: Image
        :param shiny: Boolean
        :param gold: Int
        """
        self.nom = nom
        self.rarete = rarete
        self.att = att
        self.img = img
        self.atribut = shiny
        self.gold = gold


    def setNom(self, nom):
        self.nom = nom

    def setRarete(self, rar):
        self.rarete = rar

    def setAtt(self, att):
        self.att = att

    def setImg(self, img):
        self.img = img

    def setAtributs(self, shiny):
        self.atribut = shiny

    def setGold(self, gold):
        self.gold = gold

    def getAtt(self):
        return self.att

    def getGold(self):
        return self.gold

    def getRarete(self):
        return self.rarete
    def getNom(self):
        return self.nom

    def getAtribut(self):
        return self.atribut
    def getImg(self):
        return self.img


    def affichage (self, surface, font, color) :
        rarete_display = font.render(str(self.rarete["rar"]), 1, color)
        name_display = font.render(str(self.nom), 1, color)
        deg_display = font.render("Attaque : " + str(self.getAtt()), 1, self.atribut[3])

        if self.atribut[2] == True:
            shiny_display = font.render("*shiny*", 1, color)
            surface.blit(shiny_display, (int(wSurface/2.7), int(hSurface/1.9)))

        surface.blit(rarete_display, (int(wSurface/2.8), int(hSurface/7.3)))
        surface.blit(name_display, (int(wSurface/2.7), int(hSurface/2.3)))
        surface.blit(deg_display, (int(wSurface/2.7), int(hSurface/2.1)))

        try :
            surface.blit(self.img, (int(wSurface/4), int(hSurface/6.6)))
        except TypeError :
            return 0



            #i = list.index(self)
            #for i in range(len(list)) :



    def affichTab (self, list) : # Affiche dans le terminal, voir <README>

        i = 0
        while i < len(list) :
            print list[i].nom, list[i].att

            i = i+1
        print ""




class case:



    def __init__(self, x, y, width, height, color=clVide):
        """
        :param x: Int
        :param y: Int
        :param width: Int
        :param height: Int
        :param color: Color
        """
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.colorBg = color
        self.rect = (self.x,self.y,self.width, self.height)


    def setX (self, x):
        self.x = x

    def setY (self, y):
        self.y = y

    def setColorBg (self, color) :
        self.colorBg = color

    def getRect(self):
        return pygame.Rect((self.x,self.y),(self.width, self.height))


    def affichage (self, surf) :
        pygame.draw.rect(surf, self.colorBg, self.rect)



class caseInv(case): # Herite de la classe case

    full = False
    wp = ""

    def __init__(self, x, y, width, height, img=""):

        case.__init__(self,x,y,width,height)
        self.img = img
        self.delete = False

    def setImg(self, img):
        self.img = pygame.transform.scale(img, (wSurface/5, wSurface/5))

    def addArme (self, arme) :
        self.full = True
        self.wp = arme


    def removeArme (self) :
        self.full = False
        self.wp = ""
        self.img = ""
        self.delete = True

    def getArme (self) :
        if self.full == True :
            return self.wp
        else :
            return self.wp


    def affichage(self, surface) : # Redefinition de la methode affichage
        case.affichage(self, surface)
        if self.delete == False :
            if self.full == True :
                newWp = pygame.transform.scale(self.wp.img, (int(self.width),int(self.width)))
                surface.blit(self.img, (self.x, self.y))
                surface.blit(newWp, (self.x, self.y))




