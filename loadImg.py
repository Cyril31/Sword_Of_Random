from pygame import*
import pyganim
import platform
from os import walk, mkdir

path = "img/saveWeaps/weap"


if platform.system() == "Darwin" :
    wSurface = 432
    hSurface = 768
    surface = display.set_mode((wSurface, hSurface))
else :
    surface = display.set_mode((0,0),FULLSCREEN)
    wSurface = surface.get_width()
    hSurface = surface.get_height()


imgInv = image.load("img/inv.png")
imgInv = transform.scale(imgInv, (wSurface, hSurface))
imgUp = image.load("img/up.png")
imgUp = transform.scale(imgUp, (wSurface, hSurface))
imgFusion = image.load("img/fusion.png")
imgFusion = transform.scale(imgFusion, (wSurface, hSurface))
imgBg = image.load("img/background.png")
imgBg = transform.scale(imgBg, (wSurface, hSurface))

imgGold = image.load("gold.png")
fdAff = image.load("img/invAff.png")
fdAff = transform.scale(fdAff, (int(wSurface/1.5), int(hSurface/1.9)))
fd = image.load("img/black.png")
fd = transform.scale(fd, (wSurface, hSurface))
transp = image.load("img/Transparent.png")
transp = transform.scale(transp, (wSurface, hSurface))


wCoffre = int(wSurface/1.8)
hCoffre = int(hSurface/5.5)
img_chest1 = image.load('img/chest0.png')
img_chest1 = transform.scale(img_chest1, (wCoffre, hCoffre))



sht = image.load("SHT.png")

bgCom = image.load("img/bgCom.png")
bgPc = image.load("img/bgPc.png")
bgRare = image.load("img/bgRare.png")
bgEpic = image.load("img/bgEpic.png")
bgLeg = image.load("img/bgLeg.png")

fondInv = [bgCom, bgPc, bgRare,bgEpic, bgLeg]


fdPc = image.load("img/fdPc.png")
fdPc= transform.scale(fdPc, (wSurface, hSurface))
fdRare = image.load("img/fdRare.png")
fdRare= transform.scale(fdRare, (wSurface, hSurface))
fdEpic = image.load("img/fdEpic.png")
fdEpic= transform.scale(fdEpic, (wSurface, hSurface))
fdLeg = image.load("img/fdLeg.png")
fdLeg= transform.scale(fdLeg, (wSurface, hSurface))

fondCoffre = [transp, fdPc, fdRare,fdEpic, fdLeg]

try:
   with open("img/saveWeaps/weap00.png") : pass
except IOError:
    listeWeap = []
    mkdir("img/saveWeaps")
    for (repertoire, sousRepertoires, fichiers) in walk("img/weaps"):
        listeWeap.extend(fichiers)

    for i in range(len(listeWeap)) :
        imageWeap = image.load("img/weaps/"+listeWeap[i])
        imageWeap =  transform.scale(imageWeap, (wSurface/2, wSurface/2))
        image.save(imageWeap,"img/saveWeaps/"+listeWeap[i])




img1 = image.load(path+'00.png')
img2 = image.load(path+'01.png')
img3 = image.load(path+'02.png')
img4 = image.load(path+'03.png')
img5 = image.load(path+'04.png')
img6 = image.load(path+'05.png')
img7 = image.load(path+'06.png')
img8 = image.load(path+'07.png')
img9 = image.load(path+'08.png')
img10 = image.load(path+'09.png')

img11= image.load(path+'10.png')
img12 = image.load(path+'11.png')
img13 = image.load(path+'12.png')
img14 = image.load(path+'13.png')
img15 = image.load(path+'14.png')
img16 = image.load(path+'15.png')
img17 = image.load(path+'16.png')
img18 = image.load(path+'17.png')
img19 = image.load(path+'18.png')
img20 = image.load(path+'19.png')
img21 = image.load(path+'20.png')
img22 = image.load(path+'21.png')
img23= image.load(path+'22.png')
img24= image.load(path+'23.png')
img25= image.load(path+'24.png')
vit = 100
chestCom = pyganim.getImagesFromSpriteSheet('img/anim_com.png', rows = 5, cols = 2)
frame1 = list(zip(chestCom,[ vit,vit,vit,vit,vit,vit,vit,vit,100000000]))
chestCom = pyganim.PygAnimation(frame1)
chestCom.scale( (int(wSurface/1.8), int(hSurface/5.5)))

chestPc = pyganim.getImagesFromSpriteSheet('img/anim_pc.png', rows = 5, cols = 2)
frame2 = list(zip(chestPc,[ vit,vit,vit,vit,vit,vit,vit,vit,100000000]))
chestPc= pyganim.PygAnimation(frame2)
chestPc.scale( (int(wSurface/1.8), int(hSurface/5.5)))

chestRare = pyganim.getImagesFromSpriteSheet('img/anim_rare.png', rows = 5, cols = 2)
frame5 = list(zip(chestRare,[ vit,vit,vit,vit,vit,vit,vit,vit,100000000]))
chestRare = pyganim.PygAnimation(frame5)
chestRare.scale( (int(wSurface/1.8), int(hSurface/5.5)))

chestEpic = pyganim.getImagesFromSpriteSheet('img/anim_epic.png', rows = 5, cols = 2)
frame3 = list(zip(chestEpic,[ vit,vit,vit,vit,vit,vit,vit,vit,100000000]))
chestEpic = pyganim.PygAnimation(frame3)
chestEpic.scale( (int(wSurface/1.8), int(hSurface/5.5)))

chestLeg = pyganim.getImagesFromSpriteSheet('img/anim_leg.png', rows = 5, cols = 2)
frame4 = list(zip(chestLeg,[ vit,vit,vit,vit,vit,vit,vit,vit,100000000]))
chestLeg = pyganim.PygAnimation(frame4)
chestLeg.scale( (int(wSurface/1.8), int(hSurface/5.5)))


animCoffre = [chestCom, chestPc, chestRare, chestEpic, chestLeg]