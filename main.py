"""

Second version of "Sword of Random"

Spear Hit Target - All right reserved

author: Cyril Anaclet

version: 0.2

last modification : 14/07/2018

"""




from __future__ import division # permet une divison plus precise ( barre de vie )
try:
    import pygame_sdl2
    pygame_sdl2.import_as_pygame()
except ImportError:
    pass
import pygame

from opt import option
from fonctions import*





pygame.init() # Pygame initiatlisation



# --------------------------------
# -         *COLORS*             -
# --------------------------------
black = (0,0, 0)
white = (255,255,255)
grey = (125,135,120)

if platform.system() == "Darwin" :
    wSurface = 432 #screen /2.5
    hSurface = 768
    surface = pygame.display.set_mode((wSurface, hSurface))
else :
    surface = pygame.display.set_mode((0,0),FULLSCREEN)
    wSurface = surface.get_width()
    hSurface = surface.get_height()




fontPixel = pygame.font.Font('Minecraft.ttf', int(wSurface/20)) # Pixel font (to change )
pygame.display.set_caption("Sword of Random")
horloge = pygame.time.Clock()


x_coffre = wSurface/4
y_coffre = hSurface/1.5

rect = pygame.Rect((x_coffre,y_coffre), (wCoffre,hCoffre))


# --------------------------------
# -      *CASES ONGLETS*         -
# --------------------------------
wCase = wSurface/4
hCase = hSurface/8

caseBag = case(0,hSurface- hCase,wCase,hCase)
caseUp = case(wCase,hSurface-hCase,wCase,hCase)
caseFusion = case(2*wCase,hSurface-hCase,wCase,hCase)


bag = []
equip = []
nbCoffre = [5]

# --------------------------------
# -           *ARMES*            -
# --------------------------------

Armes = [
    # -----------------------*COMMUN*------------------------------------
    {"nom": "Marteaux","Att" :5,  "img":img8},
    {"nom": "Baton", "Att" :7,  "img":img12},
    {"nom": "Torche" ,"Att" :9, "img":img4},
    {"nom": "Epee bois","Att" :11,  "img":img9},
    {"nom": "Dagues" ,"Att" :13,  "img":img14},


    # -----------------------*PEU COMMUN*--------------------------------
    {"nom": "Fauche", "Att": 15,  "img": img13},
    {"nom": "Griffes", "Att": 17,  "img": img18},
    {"nom": "Blue Wind", "Att": 19, "img": img16},
    {"nom": "Kunais", "Att": 21,  "img": img24},
    {"nom": "Blue Blow", "Att": 23,  "img": img22},

    # --------------------------*RARE*-----------------------------------
    {"nom": "Katana", "Att": 25,  "img": img1},
    {"nom": "Spikes", "Att": 27,  "img": img3},
    {"nom": "Hachettes", "Att": 29, "img": img5},
    {"nom": "Hammer", "Att": 31, "img": img23},
    {"nom": "Galaxy Sword", "Att": 33, "img": img10},

    # -------------------------*EPIQUE*----------------------------------
    {"nom": "Spider", "Att": 35, "img": img11},
    {"nom": "Cristal Wand", "Att": 37,"img": img25},
    {"nom": "Espadon", "Att": 39, "img": img7},
    {"nom": "Cutter", "Att": 41, "img": img17},
    {"nom": "Lightning Sword", "Att": 43, "img": img21},

    # -----------------------*LEGENDAIRE*---------------------------------
    {"nom": "Sorbet", "Att": 45,"img": img2},
    {"nom": "Lunar destroyer", "Att":47,"img": img6},
    {"nom": "Dragon Bow", "Att": 49,"img": img19},
    {"nom": "Magic Spear", "Att": 51, "img": img15},
    {"nom": "Haunted Sword", "Att": 53,"img": img20},

]


# --------------------------------
# *         PROBABILITY          *
# --------------------------------
rarete = [

    {"rar": "COMMUN", "inter": 0, "index": 0, "gold": 5},

    {"rar": "PEU COMMUN", "inter": 5, "index": 1, "gold": 10},

    {"rar": "RARE", "inter": 10, "index": 2, "gold": 20},

    {"rar": "EPIQUE", "inter": 15, "index": 3, "gold": 40},

    {"rar": "LEGENDAIRE", "inter": 20, "index": 4, "gold": 80}
]

# ---------------------
# COMMUN : 50%
# PEU COMMUN : 30%
# RARE : 15%
# EPIC : 4.5%
# LEGENDAIRE : 0.5%
# ---------------------
p = [0.5,0.3,0.15,0.045,0.005]


# --------------------------------
# *          INVENTORY           *
# --------------------------------
depX = wSurface/20
ecartX = wSurface/4.3

depY = hSurface/1.9
ecartY = hSurface/7

widthInv = wSurface/5
heightInv = wSurface/5


inv = [
    caseInv(depX, depY, widthInv, heightInv),
    caseInv(depX + ecartX, depY, widthInv, heightInv),
    caseInv(depX + 2*ecartX, depY, widthInv, heightInv),
    caseInv(depX + 3*ecartX, depY, widthInv, heightInv),

    caseInv(depX, depY + ecartY, widthInv, heightInv),
    caseInv(depX + ecartX, depY + ecartY, widthInv, heightInv),
    caseInv(depX + 2*ecartX, depY + ecartY, widthInv, heightInv),
    caseInv(depX + 3*ecartX, depY + ecartY, widthInv, heightInv)

]

nbInv = [inv]

caseEquip = caseInv( depX, hSurface/6, widthInv,heightInv)

# --------------------------------
# *             MAIN             *
# --------------------------------
def main():

    game_over = False

    img_chest = image.load('img/chest0.png')
    img_chest = transform.scale(img_chest, (wCoffre, hCoffre))

    i = 0
    ajoutTab = False
    fdWP = transp
    newWP = Arme()
    bool = False

    while not game_over:

        for eve in pygame.event.get():
            if eve.type == pygame.QUIT:
                game_over = True


            if eve.type == USEREVENT + 1 :
                bool = True
                time.set_timer(USEREVENT + 1, 0)


            # -- CLick on CHEST -------------------------------------
            if eve.type == pygame.MOUSEBUTTONDOWN and click( eve.pos, rect):
                if sacPlein(bag, nbInv) == False :#and nbCoffre[0] >= 1:
                        time.set_timer(USEREVENT + 1, 1000)
                       # i = 0
                        ajoutTab = True
                        bool = False
                        nbCoffre[0] -= 1
                        rarete_wp = tirage_rarete(rarete,p)
                        dic = tirage_arme(Armes,rarete_wp)
                        atributs = tirage_add(dic, rarete_wp)

                        newWP = Arme(dic["nom"], rarete_wp, atributs[0], dic["img"], atributs, atributs[1])

                        fdWP = fondCoffre[rarete_wp["index"]]
                        chestWP = animCoffre[rarete_wp["index"]]
                        img_chest = chestWP
                        img_chest.stop()
                        img_chest.play()

            # -- CLick elsewhere than CHEST ------------------------------
            if eve.type == pygame.MOUSEBUTTONDOWN and not click( eve.pos, rect):
                # reset attributs
                newWP = Arme()
                img_chest = img_chest1


            # -- CLick on BAG -----------------------------------------------------
            if eve.type == pygame.MOUSEBUTTONDOWN and click( eve.pos , caseBag.getRect()):
                sac()
                newWP = Arme()
                img_chest = img_chest1

            # -- CLick on UPGRADE ----------------------------------------------
            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseUp.getRect()):
                upgrade()

            # -- CLick on FUSION --------------------------------------------------
            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseFusion.getRect()):
                fusion()

            if eve.type == pygame.KEYDOWN  and eve.key == pygame.K_a:
                option(newWP, bag, nbInv, p, nbCoffre)


            if eve.type == pygame.KEYDOWN and eve.key == pygame.K_w:
                surf = display.get_surface().copy()
                settings(surf, surface)


            if eve.type == pygame.KEYDOWN and eve.key == K_b:
                clicker()

#     ---------------------------------------------
#     -                 *AFFICHAGE*               -
#     ---------------------------------------------

        surface.fill(grey)
        surface.blit(imgBg, (0,0))

        #disp = fontPixel.render(str(nbCoffre[0]), 2, red)
        disp = fontPixel.render(str(horloge.get_fps()), 2, red)
        surface.blit(disp, (int(wSurface/6), int(hSurface/1.5)))

        affPuissance(fontPixel,surface,white, equip)
        affGold(fontPixel, surface,white, imgGold,totGold[0])
        try:
            surface.blit(img_chest, (x_coffre, y_coffre))

        except TypeError:
            img_chest.blit(surface, (x_coffre, y_coffre))

            if bool == True :

                newWP.affichage(surface, fontPixel, white)
                blit_alpha(surface,fdWP,(0,0),70)
                if ajoutTab == True :
                    addTab(newWP, bag, nbInv)
                    ajoutTab = False

        pygame.display.update()
        horloge.tick(30)    #FPS


# --------------------------------
# *            TABS              *
# --------------------------------
def sac() :
    game_over = False
    newWP = Arme("", "", "", "", "")
    sac = 0
    x = 0

    while not game_over:


        for eve in pygame.event.get():

            if eve.type == pygame.QUIT:
                game_over = True
            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseBag.getRect()):
               game_over = True


            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseUp.getRect()):
                game_over = True
                upgrade()

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseFusion.getRect()):
                game_over = True
                fusion()

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseEquip.getRect()):
                if len(equip) > 0 :
                    if sacPlein(bag, nbInv) == False :
                        addTab(caseEquip.getArme(), bag, nbInv)
                        caseEquip.removeArme()
                        caseEquip.delete = False
                        del equip[0]
                        x = 0


            if eve.type == pygame.KEYDOWN  and eve.key == pygame.K_a:
                option(newWP, bag, nbInv,p, nbCoffre)

            if eve.type == pygame.KEYDOWN and eve.key == pygame.K_s:
                if sac < len(nbInv) -1:
                    sac += 1
                else :
                    sac = 0



            if eve.type == pygame.KEYDOWN and eve.key == pygame.K_w:
                surf = display.get_surface().copy()
                settings(surf, surface)

            for case in nbInv[sac]:
                    if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, case.getRect()):
                        if case.full == True :
                            if case.delete == False :

                                bg = display.get_surface().copy()
                                affTotal(bg, case, equip,surface, fontPixel, white, bag, nbInv, sac)

        # place toute les armes dans l'inventaire
        if addSac(x, bag, nbInv) == True:
            x += 1
            addSac(x, bag, nbInv)

        if len(equip) > 0 :
            caseEquip.addArme(getEquip(equip))
            caseEquip.setImg(fondInv[getEquip(equip).getRarete()["index"]])

        surface.fill((100,100,0))
        surface.blit(imgInv, (0,0))
        affPuissance(fontPixel,surface,white,equip)

        try:
            for c in nbInv[sac] :
                if c.getArme().nom != "vide":
                    c.delete = False
                    c.setImg(fondInv[c.getArme().getRarete()["index"]])
        except AttributeError:
            pass

        for c in nbInv[sac] :
            c.affichage(surface)

        caseEquip.affichage(surface)


        pygame.display.flip()

pv = [180, 180] # pv[0] = nombre de vie / pv[1] nombre de vie max
def clicker() :
    fin = False
    y = pv[1]

    while not fin:
        for eve in pygame.event.get():
            if eve.type == pygame.QUIT:
                pv[1] = y
                fin = True

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseUp.getRect()):
                pv[1] = y
                fin = True

            if eve.type == pygame.MOUSEBUTTONDOWN :
                pv[0] -= getEquip(equip).getAtt()


        if pv[0] <= 0 :
            nbCoffre[0] += 1
            y += 50
            pv[0] = y
            pv[1] = y

        propVie = pv[0] / pv[1]

        #disp = fontPixel.render(str(pv[0]), 1, white)
        disp = fontPixel.render(str(pv[0]), 1, white)

        surface.fill((0, 0, 0))
        surface.blit(imgBg, (0, 0))
        affPuissance(fontPixel, surface, white, equip)
        barreVie(surface, propVie)
        surface.blit(disp, (100, 100))
        pygame.display.flip()


def upgrade() :
    fin = False

    while not fin:
        for eve in pygame.event.get():
            if eve.type == pygame.QUIT:
                fin = True
            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseUp.getRect()):
                fin = True

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseBag.getRect()):
                fin = True
                sac()

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseFusion.getRect()):
                fin = True
                fusion()




        surface.fill((0, 0, 0))
        surface.blit(imgUp, (0, 0))
        affPuissance(fontPixel, surface, white, equip)
        pygame.display.flip()


def fusion():
    fin = False

    while not fin:
        for eve in pygame.event.get():
            if eve.type == pygame.MOUSEBUTTONDOWN:
                fin = True

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseBag.getRect()):
                fin = True
                sac()


            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseUp.getRect()):
                fin = True
                upgrade()



        surface.fill((0, 0, 0))
        surface.blit(imgFusion, (0, 0))
        affPuissance(fontPixel, surface, white, equip)
        pygame.display.flip()


#loading(surface)
if __name__ == "__main__" :
    main()




