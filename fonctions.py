
import random
from loadImg import*
from classArme import*


red = (255,0,0)
blue = (0,0,255)
white = (255,255,255)
totGold = [0]


def tirage_rarete(tabRar, per):
    total = 0
    i = 0
    n = random.random()

    for prob in per :
        total += prob
        if total >= n :
            return tabRar[i]
        else :
            i += 1

def tirage_arme(dicArme, rar):
    nb = random.randint(rar["inter"],(rar["inter"]+4) )
    return dicArme[nb]

def tirage_add(dic, rar) :
    shiny = False
    color = white
    att = random.randint(dic["Att"]-3,dic["Att"]+3 )
    if att == dic["Att"]+3 :
        color = red
    if att == dic["Att"]-3 :
        color = blue

    if random.randint(1,10) == 5 :
        shiny = True
    gold = rar["gold"] + att / 2

    return att, gold, shiny, color

def affPuissance(font,surface, color, equipement) :
    txt_display = font.render("Puissance :", 1, color)
    if len(equipement) > 0 :
        att_display = font.render(str(getEquip(equipement).getAtt()), 1, color)
    else :
        att_display = font.render(str(getEquip(equipement)), 1, color)

    surface.blit(txt_display, (int(wSurface/4.5), int(hSurface/25)))
    surface.blit(att_display, (int(wSurface/2), int(hSurface/25)))

def affGold(font,surface,color, img, totalGold) :
    nb_display = font.render(str(totalGold), 1, color)

    surface.blit(nb_display, (int(wSurface/1.3), int(hSurface/25)))
    surface.blit(img, (int(wSurface/1.2), int(hSurface/33)))


def addGold(nb) :
    nb += totGold[0]
    del totGold[0]
    totGold.append(nb)


def addTab(wp, sac, nbInv):
    i = 0
    b = False

    if len(sac) >= 1 :
        #ici le pb, integre le nbInv
        while i < len(sac) and b == False:
            if sac[i].nom == "vide" :
                sac[i].setNom(wp.getNom())
                sac[i].setRarete(wp.getRarete())
                sac[i].setAtt(wp.getAtt())
                sac[i].setImg(wp.getImg())
                sac[i].setAtributs(wp.getAtribut())
                sac[i].setGold(wp.getGold())
                b = True
            else :
                i += 1
        if b == False :
            sac.append(wp)


    else :
        sac.append(wp)

def delTab(wp, sac):
    for i in range(len(sac)):
        if sac[i] == wp :
            sac[i].setNom("vide")
            sac[i].setRarete("")
            sac[i].setAtt("")
            sac[i].setImg(transp)
            sac[i].setAtributs("")
            sac[i].setGold("")




def getEquip(equipement) : # retourne l'arme equiper
    if len(equipement) > 0:
        return equipement[0]
    else :
        return 0

def setEquip(wp, equipement) :
    if len(equipement) >= 1: # 1 est le nombre d'emplacement pouvant etre equipe
        del equipement[0]
    newWP = Arme(wp.getNom(),wp.getRarete(),wp.getAtt(),wp.getImg(),wp.getAtribut(),wp.getGold())
    equipement.append(newWP)


def blit_alpha(target, source, location, opacity):
    x = location[0]
    y = location[1]
    temp = Surface((source.get_width(), source.get_height())).convert()
    temp.blit(target, (-x, -y))
    temp.blit(source, (0, 0))
    temp.set_alpha(opacity)
    target.blit(temp, location)


def settings(fond, surface) :
    game_over = False


    while not game_over:
        for eve in event.get():
            if eve.type == pygame.QUIT:
                game_over = True

        surface.fill((100,100,100))
        surface.blit(fond, (0,0))
        blit_alpha(surface,fd,(0,0), 150)
        surface.blit(img1, (0,0))
        pygame.display.flip()


def loading(surface) :
    fin = False

    time.set_timer(USEREVENT + 1, 5)
    i = 0
    while not fin :
        for eve in pygame.event.get():
            if eve.type == MOUSEBUTTONDOWN:
                fin = True
            if eve.type == USEREVENT + 1 :
                i += 1

        if time.get_ticks() >= 5000:
           fin = True

        surface.fill((0, 0, 0))
        blit_alpha(surface,sht, (0,0), i)
        display.flip()


def click ( cur, rect) :
    if rect.collidepoint(cur) :
        return True


def addSac(x, bag, nbInv) :
    i = 0
    for j in range(8*x,len(bag)):
            try:
                if nbInv[x][i].full == False:
                  # if nbInv[x][i].delete == False :
                    try :
                        nbInv[x][i].addArme(bag[j])
                        nbInv[x][i].setImg(fondInv[bag[j].getRarete()["index"]])
                    except TypeError or AttributeError:
                        pass
                i += 1
            except IndexError:
                return True


def sacPlein(sac, nbInv):
    y = 0
    for i in range(len(sac)):
       if sac[i].getNom() != "vide" :
           y += 1

    if y == 8*len(nbInv) :
        return True
    else :
        return False



def affTotal(background, caseArme, equip,surface, font, color, sac, nbInv, sacAct) :
    caseAff = case(int(wSurface/6),int(wSurface/5),int(wSurface/3.5),int(wSurface/3))
    caseEquip = case(int(wSurface/6),int(hSurface/1.8),int(wSurface/2.9),int(hSurface/15))
    caseVendre = case(int(wSurface/2.9)+int(wSurface/6), int(hSurface/1.8), int(wSurface/2.9), int(wSurface/6))
    newImg= pygame.transform.scale(caseArme.getArme().getImg(), (int(wSurface/2.5), int(wSurface/2.5)))
    rarete_display = font.render(str(caseArme.getArme().getRarete()["rar"]), 1, color)
    name_display = font.render(str(caseArme.getArme().getNom()), 1, color)
    deg_display = font.render("Attaque : " + str(caseArme.getArme().getAtt()), 1, caseArme.getArme().getAtribut()[3])
    equip_display = font.render("EQUIPER", 1, color)
    vendre_display = font.render("VENDRE", 1, color)
    ancEquip = 0
    b = False
    fin = False
    while not fin:
        for eve in pygame.event.get():
            if eve.type == pygame.MOUSEBUTTONDOWN and not click(eve.pos, caseAff.getRect()):
                fin = True

            if eve.type == pygame.MOUSEBUTTONDOWN and click(eve.pos, caseEquip.getRect()):
                fin = True
                if len(equip) >= 1 and equip[0].getNom != "vide":
                    ancEquip = equip[0]
                    b = True

                setEquip(caseArme.getArme(), equip)
                delTab(caseArme.getArme(), sac)
                caseArme.removeArme()
                caseArme.delete = True

                if b == True :
                    addTab(ancEquip, sac, nbInv)



            if eve.type == pygame.MOUSEBUTTONDOWN and  click(eve.pos, caseVendre.getRect()):
                fin = True
                addGold(caseArme.getArme().getGold())

                delTab(caseArme.getArme(),sac)
                caseArme.removeArme()
                caseArme.delete = True


        surface.fill((0,0,0))
        surface.blit(background,(0,0))
        blit_alpha(surface,fd,(0,0), 100)
        surface.blit(fdAff, (caseAff.x, caseAff.y))
        surface.blit(equip_display,(caseEquip.x + int(caseAff.width/4),caseEquip.y + int(caseAff.width/4)))
        surface.blit(vendre_display, (caseVendre.x + int(caseAff.width/4), caseVendre.y+ int(caseAff.width/4)))
        surface.blit(rarete_display, (caseAff.x+ int(caseAff.width/1.5), caseAff.y+int(caseAff.height/0.8)))
        surface.blit(name_display, (caseAff.x+ int(caseAff.width/1.5), caseAff.y+int(caseAff.height/0.65)))
        surface.blit(deg_display, (caseAff.x+ int(caseAff.width/1.5), caseAff.y+int(caseAff.height/0.55)))
        surface.blit(newImg, (caseAff.x+ int(caseAff.width/2), caseAff.y))
        try :
            if caseArme.getArme().getAtribut()[2] == True:
                shiny_display = font.render("*shiny*", 1, color)
                surface.blit(shiny_display, (caseAff.x+ int(caseAff.width/1.5), caseAff.y+int(caseAff.height/0.5)))
        except AttributeError:
            pass

        pygame.display.flip()

def barreVie(surf, prop) :
    width = 200*prop
    rectRouge = case(100, 150, width, 10, red)


    rectRouge.affichage(surf)
